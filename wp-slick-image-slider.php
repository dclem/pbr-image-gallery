<?php
/**
 * Plugin Name: Homepage Gallery Plugin
 * Plugin URI: https://www.rrpartners.com
 * Text Domain: wp-slider-pbr
 * Domain Path: /languages/
 * Description: Add artwork slider and image grid to website
 * Author: R&R Partners
 * Version: 1.0.0
 * Author URI: https://www.rrpartners.com
 *
 * @package WordPress
 * @author WP Online Support
 */

if( !defined('WPSISAC_VERSION') ) {
    define( 'WPSISAC_VERSION', '1.0.0' ); // Plugin version
}
if( !defined( 'WPSISAC_VERSION_DIR' ) ) {
    define( 'WPSISAC_VERSION_DIR', dirname( __FILE__ ) ); // Plugin dir
}
if( !defined( 'WPSISAC_URL' ) ) {
    define( 'WPSISAC_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}
if( !defined( 'WPSISAC_POST_TYPE' ) ) {
    define( 'WPSISAC_POST_TYPE', 'slick_slider' ); // Plugin post type
}

// Function file
require_once( WPSISAC_VERSION_DIR . '/includes/wpsisac-function.php' );

// Script
require_once( WPSISAC_VERSION_DIR . '/includes/class-wpsisac-script.php' );

// Post type file
require_once( WPSISAC_VERSION_DIR . '/includes/wpsisac-slider-custom-post.php' );

// Shortcode File
require_once( WPSISAC_VERSION_DIR . '/includes/shortcodes/wpsisac-carousel.php' );
require_once( WPSISAC_VERSION_DIR . '/includes/shortcodes/image-grid.php' );
require_once( WPSISAC_VERSION_DIR . '/includes/shortcodes/form.php' );

// Admin File
require_once( WPSISAC_VERSION_DIR . '/includes/admin/class-wpsisac-admin.php' );
