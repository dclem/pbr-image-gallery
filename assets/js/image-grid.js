(function($) {
  var currentPage, nextPage, year, category;

  $(document).on("click", "#clear-filter-btn", function(event) {
    $('input[name="type-option"]').attr("checked", false);
    $('input[name="year-option"]').attr("checked", false);

    $(".load-more-images").attr("data-page", 1);
    $(".load-more-images").attr("data-year", "");
    $(".load-more-images").attr("data-category", "");

    event.preventDefault();
  });

  $(document).on("click", "#filter-images-btn", function(event) {
    page = 1;

    $("#filter")
      .find("input[name=year-option]:checked")
      .each(function(i, ob) {
        year = $(ob).val();
      });

    $("#filter")
      .find("input[name=type-option]:checked")
      .each(function(i, ob) {
        category = $(ob).val();
      });

    if (!category) {
      category = "all";
    }

    $(".load-more-images").attr("data-page", page);
    $(".load-more-images").attr("data-year", year);
    $(".load-more-images").attr("data-category", category);

    // console.log("Year: " + year);
    // console.log("Category: " + category);

    $.ajax({
      url: ajaxloadmore.ajaxurl,
      type: "post",
      data: {
        action: "load_more",
        year: year,
        page: parseInt(page),
        category: category
      },
      beforeSend: function() {
        $(".loading-message").show();
      },
      success: function(html) {
        var $html = $(html);
        $("#grid").html($html);
      },
      complete: function() {
        $(".loading-message").hide();
        $("#touch").prop("checked", false);
      }
    });
  });

  $(document).on("click", ".load-more-images", function(event) {
    var $grid = $("#grid");
    var $this = $(this);

    event.preventDefault();

    currentPage = $this.attr("data-page");

    year = $this.attr("data-year");
    category = $this.attr("data-category");

    if (!category) {
      category = "all";
    }

    // console.log("Year: " + year);
    // console.log("Category: " + category);
    // console.log("Page: " + currentPage);

    $.ajax({
      url: ajaxloadmore.ajaxurl,
      type: "post",
      data: {
        action: "load_more",
        year: year,
        page: parseInt(currentPage) + 1,
        category: category,
        rel: $(this).attr("data-rel")
      },
      beforeSend: function() {
        $(".loading-message").show();
      },
      success: function(html) {
        nextPage = parseInt(currentPage) + 1;
        currentPage = $this.attr("data-page", nextPage);

        var $html = $(html);
        $grid.append($html);

        $("[data-fancybox]").fancybox({
          buttons: ["close"]
        });

        $html = null;
      },
      complete: function() {
        $(".loading-message").hide();
      }
    });
  });
})(jQuery);
