jQuery(document).ready(function($) {
	$("[data-fancybox]").fancybox({
		buttons : ['close']
	});

	// For Carousel Slider
	$( '.wpsisac-slick-wrapper' ).each(function( index ) {

		var $mainSlider   = $(this).find('.wpsisac-main-slider'),
			$thumbSlider	= $(this).find('.slider-nav'),
			mainSliderID = $mainSlider.attr('id'),
			thumbSliderID = $thumbSlider.attr('id');

			var slider_conf = $.parseJSON( $(this).closest('.wpsisac-slick-carousal-wrp').find('.wpsisac-carousal-conf').attr('data-conf'));

		$($thumbSlider).slick({
			dots			: (slider_conf.dots) == "true" ? true : false,
			arrows			: true,
			speed			: parseInt(slider_conf.speed),
			autoplay		: (slider_conf.autoplay) == "true" ? true : false,
			autoplaySpeed	: parseInt(slider_conf.autoplay_interval),
			slidesToShow	: parseInt(slider_conf.slidestoshow),
			infinite 		: true,
			asNavFor		: '#' + mainSliderID,
			focusOnSelect	: true,
			slidesToScroll	: -1,
			nextArrow		: '<button class="slick-prev slick-arrow" aria-label="Next" type="button" style="display: block;">Next</button>',
			prevArrow		: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style="display: block;">Next</button>',
			centerMode 		: (slider_conf.centermode) == "true" ? true : false,
			variableWidth 	: (slider_conf.variablewidth) == "true" ? true : false,
			mobileFirst    	: false,
			responsive 		: [{
				breakpoint 	: 1023,
				settings 	: {
					slidesToShow 	: (parseInt(slider_conf.slidestoshow) > 3) ? 3 : parseInt(slider_conf.slidestoshow),
					slidesToScroll 	: 1,
				}
			},{
				breakpoint	: 767,
				settings	: {
					slidesToShow 	: (parseInt(slider_conf.slidestoshow) > 3) ? 3 : parseInt(slider_conf.slidestoshow),
					slidesToScroll 	: 1,
					centerMode 		: (slider_conf.centermode) == "true" ? true : false,
				}
			},{
				breakpoint	: 639,
				settings	: {
					slidesToShow 	: 1,
					slidesToScroll 	: 1,
					dots 			: false,
					centerMode 		: true,
					variableWidth 	: false,
				}
			},{
				breakpoint	: 479,
				settings	: {
					slidesToShow 	: 1,
					slidesToScroll 	: 1,
					dots 			: false,
					centerMode 		: false,
					variableWidth 	: false,
				}
			},{
				breakpoint	: 319,
				settings	: {
					slidesToShow 	: 1,
					slidesToScroll 	: 1,
					dots 			: false,
					centerMode 		: false,
					variableWidth 	: false,
				}
			}]
		})
		$($mainSlider).slick({
			dots			: (slider_conf.dots) == "true" ? true : false,
			arrows			: (slider_conf.arrows) == "true" ? true : false,
			speed			: parseInt(slider_conf.speed),
			autoplay		: (slider_conf.autoplay) == "true" ? true : false,
			autoplaySpeed	: parseInt(slider_conf.autoplay_interval),
			slidesToShow	: parseInt(slider_conf.slidestoshow),
			infinite 		: true,
			// asNavFor		: '#' + thumbSliderID,
			slidesToScroll	: -1,
			nextArrow		: '<button class="slick-prev slick-arrow" aria-label="Next" type="button" style="display: block;">Next</button>',
			prevArrow		: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style="display: block;">Next</button>',
			centerMode 		: (slider_conf.centermode) == "true" ? true : false,
			variableWidth 	: (slider_conf.variablewidth) == "true" ? true : false,
			mobileFirst    	: false,
			responsive 		: [{
				breakpoint 	: 1023,
				settings 	: {
					slidesToShow 	: (parseInt(slider_conf.slidestoshow) > 3) ? 3 : parseInt(slider_conf.slidestoshow),
					slidesToScroll 	: 1,
				}
			},{
				breakpoint	: 767,
				settings	: {
					slidesToShow 	: (parseInt(slider_conf.slidestoshow) > 3) ? 3 : parseInt(slider_conf.slidestoshow),
					slidesToScroll 	: 1,
					centerMode 		: (slider_conf.centermode) == "true" ? true : false,
				}
			},{
				breakpoint	: 600,
				settings	: {
					slidesToShow 	: 1,
					slidesToScroll 	: 1,
					arrows			: true,
					dots 			: false,
					centerMode 		: false,
					variableWidth 	: false,
					adaptiveHeight  : false
				}
			}]
		});
	});

});