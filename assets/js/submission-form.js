jQuery(document).ready(function($) {

    var $radioCat = $('.cat_radio'), $form;


    $radioCat.on('change', function(){

         var selectedCat = $('input[name=art_type]:checked').val();

         if (selectedCat === '2d-art-can') {
            $(".jpg-select").show();
            $(".gif-select").hide();
            $(".video-select").hide();
            $(".category_info").hide();
            
         } else if (selectedCat === 'digital-media') {
            $(".category_info").hide();
            $(".jpg-select").hide();
            $(".gif-select").show();
            $(".video-select").show();
         }
         
    });


    $form = $("#pbr-art-form");

    $form.on("change", "#file-upload-field", function(){ 
        $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') );
        $(".category-required-label").css('visibility', 'hidden');
    });

    $("#btn-submit-art").on('click', function(e) {

        console.log('clicked');
        // var selectedCat = $('input[name=art_type]:checked').val();

        // if (selectedCat === 'digital-media') {
        //     var selectedFile = $('input[name=gif_upload_field]').val();        
        // } else if (selectedCat === '2d-art-can') {
        //     var selectedFile = $('input[name=jpg_upload_field]').val();
        // }

        // console.log(selectedFile);

        // if (!selectedFile) {
        //     console.log('File needs selected');
        //     $(".category-required-label").css('visibility', 'visible');
        //     return false;
        // } 

        // e.preventDefault();
        // $(this).submit();
    });

});
