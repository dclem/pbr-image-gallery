  <div class="slick-image-slide">  
	<?php

		// $videoUrl = get_post_meta( get_the_ID(),'wpsisac_slide_link', true );
		// $slider_img 	= wpsisac_get_post_featured_image( $post->ID, $sliderimage_size, true );

		$videoUrl 	= get_post_meta( get_the_ID(),'wpsisac_slide_link', true );

		if ($videoUrl) {
			
			$data_arr = getVideoImage($videoUrl);
	
			$slider_img = $data_arr['slider_img'];
			$data_src = $data_arr['data_scr'];
		
		} else {
	
			$slider_img = wpsisac_get_post_featured_image( $post->ID, $sliderimage_size, true );
			$data_src = wpsisac_get_post_featured_image( $post->ID, $sliderimage_size, true );
	
		}
		
	?>


		<div class="slick-image-slide-wrap" <?php echo $thumbs_height_css ; ?>><img src="<?php echo $slider_img; ?>" alt="<?php the_title(); ?>" /></div>		

</div>