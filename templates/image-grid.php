<div class="nav-container">

  <nav class="filter" id="filter">

    <label class="label" for="touch">
      <span class="menu-filter">Filter</span>
    </label>
    <input type="checkbox" id="touch">

    <div class="slide">

      <div class="col-1">

        <h3 class="menu-filter">Year</h3>

        <div class="radio">
          <label class="label">
            <input type="radio" name="year-option" id="year-option" value="2018">
            <span></span>
            2018
          </label>
        </div>

        <div class="radio">
          <label class="label">
            <input type="radio" name="year-option" id="year-option" value="2017">
            <span></span>
            2017
          </label>
        </div>

        <div class="radio">
          <label class="label">
            <input type="radio" name="year-option" id="year-option" value="2016">
            <span></span>
            2016
          </label>
        </div>

        <div class="radio">
          <label class="label">
            <input type="radio" name="year-option" id="year-option" value="2015">
            <span></span>
            2015
          </label>
        </div>

        <div class="radio">
          <label class="label">
            <input type="radio" name="year-option" id="year-option" value="2014">
            <span></span>
            2014
          </label>
        </div>

        <div class="radio">
          <label class="label">
            <input type="radio" name="year-option" id="year-option" value="2013">
            <span></span>
            2013
          </label>
        </div>

        <div class="radio">
          <label class="label">
            <input type="radio" name="year-option" id="year-option" value="2012">
            <span></span>
            2012
          </label>
        </div>

      </div>

      <div class="col-2">

        <h3 class="menu-filter">Category</h3>

        <div class="radio">
          <label class="label">
            <input type="radio" name="type-option" id="type-option" value="art-can">
            <span></span>
            Can Art
          </label>
        </div>

        <div class="radio">
          <label class="label">
            <input type="radio" name="type-option" id="type-option" value="2d">
            <span></span>
            2D Art
          </label>
        </div>

        <div class="radio">
          <label class="label">
            <input type="radio" name="type-option" id="type-option" value="3d">
            <span></span>
            3D Art
          </label>
        </div>

        <div class="radio">
          <label class="label">
            <input type="radio" name="type-option" id="type-option" value="Photo / Digital Media">
            <span></span>
            Photo / Digital Media
          </label>
        </div>

      </div>

      <button id="filter-images-btn" type="button" class="btn active btn-secondary">Filter</button>
      <button id="clear-filter-btn" type="button" class="btn active btn-secondary">Clear</button>

      <div style="display: block; right: 122px; bottom: 18px;" class="loading-message loader">
        <img src="<?=WPSISAC_URL . '/assets/img/loader.svg'?>">
      </div>

    </div>

  </nav>

</div>

<div class="row" id="grid">

    <?php

if ($query->have_posts()): while ($query->have_posts()): $query->the_post();

        $remove[] = "'";
        $remove[] = '"';

        $year = get_post_meta($post->ID, 'art_year', true);
        $type = get_post_meta($post->ID, 'art_type', true);
        $artist = str_replace($remove, "", get_post_meta($post->ID, 'artist_name', true));
        $title = str_replace($remove, "", get_the_title());

        $videoUrl = get_post_meta($post->ID, 'art_video_url', true);

        if ($videoUrl) {

            $data_arr = getVideoImage($videoUrl);

            $slider_img = $data_arr['slider_img'];
            $data_src = $data_arr['data_scr'];

        } else {

            $slider_img = wpsisac_get_post_featured_image($post->ID, 'large', true);
            $data_src = wpsisac_get_post_featured_image($post->ID, 'large', true);

        }

        the_content();

        ?>

				      <a class="modal-img" rel="<?=$gallery_group?>" data-fancybox="<?=$gallery_group?>" data-src="<?=$data_src;?>" data-caption="<h3 style='margin:0;'><?=$title?></h3><div><?=$artist?></div><div><?=$type?></div><div><?=$year?></div>">
				        <div class="grid-item">
				          <img src="<?=$slider_img?>" />
				        </div>
				      </a>

				    <?php

    endwhile;

endif;

?>
</div>

<div style="text-align: center; margin-top: 50px;">
  <a class="elementor-button-link elementor-button elementor-size-md load-more-images" data-year="<?=date('Y');?>" data-page="1" data-category="all" data-rel="<?=$gallery_group?>"
    href="#">Load More</a>
  <div style="display: none;" class="loading-message loader">
    <img src="<?=WPSISAC_URL . '/assets/img/loader.svg'?>">
  </div>
</div>