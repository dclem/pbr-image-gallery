<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" id="pbr-art-form" method="POST" name="pbr-art-form" enctype="multipart/form-data">

	<div class="row">

		<h3>Personal Information</h3>

		<p>Please complete the following information. All inputs marked in red are required.</p>

		<div class="row">
			<div class="six columns">
				<label for="exampleEmailInput">Full Name</label>
				<input name="artist_name" class="u-full-width" type="text" required> 
			</div>
			<div class="six columns">
				<label for="exampleEmailInput">Email Address</label>
				<input name="artist_email" class="u-full-width" type="text" required>
			</div>
		</div>
		<div class="row">
			<div class="six columns">
				<label for="">Mailing Address Line 1</label>
				<input name="artist_address_one" class="u-full-width" type="text" required>
			</div>
			<div class="six columns">
				<label for="">Mailing Address Line 2</label>
				<input name="artist_address_two" class="u-full-width" type="text">
			</div>
		</div>

		<div class="row">
			<div class="two columns">
				<label for="exampleEmailInput">City</label>
				<input name="artist_city" class="u-full-width" type="text" required>
			</div>

			<div class="two include-margin columns">
				<?=states_select('artist_state')  ?>
			</div>

			<div class="two include-margin columns">
				<label for="exampleEmailInput">Zip Code</label>
				<input name="artist_zip" class="u-full-width" type="text" required>
			</div>
			<div class="two columns">
				<label for="exampleEmailInput">Instagram Handle (@handle)</label>
				<input name="artist_instagram_handle" class="u-full-width" type="text">
			</div>
		</div>

		<div class="row">
			<div class="two columns">
				<label for="exampleEmailInput">Phone Number</label>
				<input name="artist_phone" class="u-full-width" type="text" required>
			</div>
			<div class="four offset-by-four include-margin columns">
				<label for="exampleEmailInput">Website</label>
				<input name="artist_website" class="u-full-width" type="text">
			</div>
		</div>

	</div>

	<div class="row">

		<h3>SUBMIT YOUR ARTWORK</h3>

		<div class="row">

			<div class="five columns">

				<label>Select the category your art piece belongs to</label> <span class="category-required-label" style="display:inline-block; margin-bottom: 10px; visibility: hidden; color: #B80606; font-weight: bold; letter-spacing: 1px;">** Be sure you have selected and category and artwork</span>
				<div class="radio" style="width: 50%; float: left;">
					<label>
						<input class="cat_radio" type="radio" name="art_type" value="2d-art-can" required>
						<span class="glyphicon glyphicon-ok"></span>
						2D/Art Can
					</label>
				</div>

				<div class="radio">
					<label>
						<input class="cat_radio" type="radio" name="art_type" value="digital-media" required>
						<span class="glyphicon glyphicon-ok"></span>
						Digital Media (GIF or Video)
					</label>
				</div>

			</div>
			<div class="four offset-by-one include-margin columns">
				<label for="exampleEmailInput">Artwork Title</label>
				<input name="post_title" class="u-full-width" type="text" required>
			</div>
		</div>
		<div class="row">

			<div class="four columns">

			<div class="category_info">&nbsp;</div>

				<div class="jpg-select" style="display: none;">
					<label for="exampleEmailInput">Select your JPG/JPEG Asset</label>
					<div class="file-upload-wrapper" data-text="Select your file">
						<input id="file-upload-field" name="jpg_upload_field" type="file" accept=".jpg, .jpeg" class="file-upload-field" value="">
					</div>
				</div>

				<div class="gif-select" style="display: none;">
					<label for="exampleEmailInput">Select your GIF Asset</label>
					<div class="file-upload-wrapper" data-text="Select your GIF file">
						<input id="file-upload-field" name="gif_upload_field" type="file" accept=".gif" class="file-upload-field" value="">
					</div>
				</div>

				<div class="video-select" style="display: none;">
					<h3>OR</h3>
					<label for="exampleEmailInput">Link to Vimeo or Youtube Video</label>
					<input name="video_url" class="u-full-width" type="text">
				</div>
			</div>

			<div class="six offset-by-two include-margin columns">
				<label for="exampleEmailInput">Description</label>
				<textarea name="art_description" class="u-full-width" placeholder="" id=""></textarea>
			</div>

		</div>

		<div class="row">
			<div class="columns two offset-by-six">

				<label for="exampleRecipientInput">Age</label>
			
				<div class="state-dropdown">
					<select name="artist_age" class="u-full-width" id="" required>

						<option value="">Select your Age</option>

						<?php for($i=21; $i <= 100; $i++ ) : ?>

							<option value="<?=$i?>">
								<?=$i?>
							</option>

						<?php endfor; ?>
					
					</select>
				</div>
			
			</div>

			<div class="columns three" style="margin-top: 31px;">
				<div class="checkbox">
					<label>
						<input type="checkbox" name="rules" value="" required>
						<span class="glyphicon glyphicon-ok"></span>
						Indicate you have Read the Rules
					</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="columns two offset-by-six">
				<input id="btn-submit-art" class="button-primary u-full-width" type="submit" name="submit-form" value="Submit">
			</div>
		</div>

	</div>

</form>