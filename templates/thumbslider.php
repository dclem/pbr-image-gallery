<div class="slick-image-slide <?=($captions == 'true') ? 'no-overlay' : '' ?>">

<?php
	
	$options 	= '{ "buttons" : "close" }';

	$videoUrl 	= get_post_meta( get_the_ID(),'wpsisac_slide_link', true );

	if ($videoUrl) {
		
		$data_arr = getVideoImage($videoUrl);

		$slider_img = $data_arr['slider_img'];
		$data_src = $data_arr['data_scr'];
	
	} else {

		$slider_img = wpsisac_get_post_featured_image( $post->ID, $sliderimage_size, true );
		$data_src = wpsisac_get_post_featured_image( $post->ID, $sliderimage_size, true );

	}
	
?>

<div class="slick-image-slide-wrap" <?php echo $slider_height_css ; ?>>
	<a class="modal-img" data-src="<?php echo $data_src; ?>" rel="<?=$gallery_group?>" data-options="<?=$options ?>" data-caption="<h3><?=the_title(); ?></h3><?=the_content(); ?>" data-fancybox="<?=$gallery_group?>">
	
		<?php if ($videoUrl) : ?>
			<div class="title-overlay" style=""><h3><?php the_title(); ?></h3></div>
			<div class="play-btn-overlay"><img src="<?= WPSISAC_URL . '/assets/img/play.svg' ?>"></div>
		<?php endif; ?>

		<img src="<?php echo $slider_img; ?>" alt="<?php the_title(); ?>" />
	
	</a>
</div>

<?php if ($captions == 'true') : ?>

	<div class="art-can-date">
		<h2><?= get_the_date("Y"); ?></h2>
		<div class="can-subcontent">
			<p><?php the_title(); ?></p>
			<?=the_content(); ?>
		</div>
	</div>

<?php endif; ?>

</div>
