<div class="slick-image-slide">  
	<?php
		$options = '{ "buttons" : "close" }';

		$sliderurl = get_post_meta( get_the_ID(),'wpsisac_slide_link', true );
		$slider_img 	= wpsisac_get_post_featured_image( $post->ID, $sliderimage_size, true );
		echo ($sliderurl !='' ? '<a href="'.$sliderurl.'">' : '');
	?>
	<div class="slick-image-slide-wrap" <?php echo $slider_height_css ; ?>>
		<a class="modal-img" data-src="<?php echo $slider_img; ?>" data-options="<?=$options ?>" data-caption="<h3><?=the_title(); ?></h3><?=the_content(); ?>" data-fancybox="gallery">
			<img src="<?php echo $slider_img; ?>" alt="<?php the_title(); ?>" />
		</a>
	</div>
</div>
