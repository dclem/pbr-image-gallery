<?php
/**
 * Plugin generic functions file
 *
 * @package WP Slick Slider and Image Carousel Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Function to get plugin image sizes array
 * 
 * @package WP Slick Slider and Image Carousel
 * @since 1.2.2
 */
function wpsisac_get_unique() {
  static $unique = 0;
  $unique++;

  return $unique;
}
/**
 * Function to get post featured image
 */

function wpsisac_get_post_featured_image( $post_id = '', $size = 'full') {
    $size   = !empty($size) ? $size : 'full';
    $image  = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size );

    if( !empty($image) ) {
        $image = isset($image[0]) ? $image[0] : '';
    }
    return $image;
}

/**
 * Function to get shortcode designs
 * 
 * @package WP Slick Slider and Image Carousel
 * @since 1.2.5
 */
function wpsisac_slider_views() {
    $views_arr = array(
        'thumbslider'  	=> __('Slider with Thumbs View', 'wp-slick-slider-and-image-carousel'),
        'cardsgrid'  	=> __('Cards Grid', 'wp-slick-slider-and-image-carousel'),
        'design-3'  	=> __('Design 3', 'wp-slick-slider-and-image-carousel'),
        'design-4' 		=> __('Design 4', 'wp-slick-slider-and-image-carousel'),
        'design-5' 		=> __('Design 5', 'wp-slick-slider-and-image-carousel'),
        'design-6' 		=> __('Design 6', 'wp-slick-slider-and-image-carousel'),       
	);
	return apply_filters('wpsisac_slider_views', $views_arr );
}


function getVideoImage ($videoUrl) {
    
    if (preg_match_all('#(http://www.youtube.com)?/(v/([-|~_0-9A-Za-z]+)|watch\?v\=([-|~_0-9A-Za-z]+)&?.*?)#i', $videoUrl, $output)) {
          
        $videoId = $output[4][0];
        $slider_img = "https://i.ytimg.com/vi/$videoId/maxresdefault.jpg";
        $data_src = "https://www.youtube.com/embed/$videoId?rel=0&amp;showinfo=0";

      } 
      
      if (preg_match_all('#https?://(player\.)?vimeo\.com(/video)?/(\d+)#i',$videoUrl, $output)) {

        $videoId = $output[3][0];
        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$videoId.php"));
        $slider_img = $hash[0]['thumbnail_large'];        
        $data_src = "https://vimeo.com/$videoId";

      }

    return array (
        'data_scr' => $data_src,
        'slider_img' => $slider_img
    );

}