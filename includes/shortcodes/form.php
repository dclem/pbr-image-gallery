<?php

function get_wpsisac_submission_form( $atts, $content = null ){

	$view_file 	= WPSISAC_VERSION_DIR . '/templates/form.php';

    extract(
		shortcode_atts(
		array(
			"limit"     		=> '-1'
	), $atts));

	ob_start();

	if (isset($_POST['submit-form'])) {
		
		global $wpdb;

		$artist = $_POST['artist_name'];

		$data = array(
			'post_title'=> addslashes($_POST['post_title']), 
			'post_type'=>'pbrart', 
			'post_content'=>'',
			'post_status' => 'draft',
			'post_category' => array(14), 
			'meta_input' => array(
				'artist_name' => addslashes($_POST['artist_name']),
				'art_type' => $_POST['art_type'],
				'art_year' => date('Y'),
				'art_description' => addslashes($_POST['art_description']),
				'art_video_url' => $_POST['video_url'],
				'artist_age' => $_POST['artist_age'],
				'artist_address' => $_POST['artist_address_one'],
				'artist_address_two' => $_POST['artist_address_two'],
				'artist_city' => $_POST['artist_city'],
				'artist_state' => $_POST['artist_state'],
				'artist_zip' => $_POST['artist_zip'],
				'artist_email' => $_POST['artist_email'],
				'artist_phone' => $_POST['artist_phone'],
				'artist_website' => $_POST['artist_website'],
				'artist_web' => $_POST['artist_website'],
				'artist_instagram_handle' => $_POST['artist_instagram_handle'],
				// 'artist_twitter_handle' => $_POST['artist_twitter_handle'],
			)
		);

		$post_id = wp_insert_post($data);
		
		
		if (!empty($_FILES['gif_upload_field']['name'])) {
			$uploadedfile = $_FILES['gif_upload_field'];
		} else if (!empty($_FILES['jpg_upload_field']['name'])) {
			$uploadedfile = $_FILES['jpg_upload_field'];
		};

		$attachment_id = upload_user_file( $uploadedfile, $post_id );
		update_field('_thumbnail_id', $attachment_id, $post_id);


		echo "<div style='background-color: #1b3e93; color: #fff; padding: 1px 32px; font-weight: bold;'><p>Thank you for your submission $artist! We will review the information you have provided us.</p></div>";

		// print_r($_POST);
		// echo 'Post ID: ' . $post_id;
		// echo 'GIF File: ' . print_r($uploadedfile);
		// echo 'JPG File: ' . print_r($uploadedfile);
		// echo'</pre>';
		 
		// exit;

	}

	include( $view_file );

	return ob_get_clean();
	
};

add_shortcode('load-submission-form','get_wpsisac_submission_form');

function upload_user_file( $file, $post_id ) {

	require_once( ABSPATH . 'wp-admin/includes/admin.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php');
	require_once( ABSPATH . 'wp-admin/includes/image.php');

	$wordpress_upload_dir = wp_upload_dir();
	// $wordpress_upload_dir['path'] is the full server path to wp-content/uploads/2017/05, for multisite works good as well
	// $wordpress_upload_dir['url'] the absolute URL to the same folder, actually we do not need it, just to show the link to file
	$i = 1; // number of tries when the file with the same name is already exists
	 
	$imageFile = $file;
	$new_file_path = $wordpress_upload_dir['path'] . '/' . $file['name'];
	$new_file_mime = mime_content_type( $file['tmp_name'] );

	// looks like everything is OK
	if( move_uploaded_file( $imageFile['tmp_name'], $new_file_path ) ) {
	 
		$upload_id = wp_insert_attachment( array(
			'guid'           => $new_file_path, 
			'post_mime_type' => $new_file_mime,
			'post_title'     => preg_replace( '/\.[^.]+$/', '', $imageFile['name'] ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		), $new_file_path );
	 
		// wp_generate_attachment_metadata() won't work if you do not include this file
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
	 
		// Generate and save the attachment metas into the database
		wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $new_file_path ) );
	 
		return $upload_id;
	}
	
}


function states_select( $name = 'niica_member_state', $user_id = 0 ) {
	// fetch the current status
	$current	= get_user_meta( $user_id, 'niica_member_state', true );
	$current	= !empty( $current ) ? esc_attr( $current ) : 'none';
	$states	= array(
		'AL' => 'Alabama',
		'AK' => 'Alaska',
		'AZ' => 'Arizona',
		'AR' => 'Arkansas',
		'CA' => 'California',
		'CO' => 'Colorado',
		'CT' => 'Connecticut',
		'DE' => 'Delaware',
		'FL' => 'Florida',
		'GA' => 'Georgia',
		'HI' => 'Hawaii',
		'ID' => 'Idaho',
		'IL' => 'Illinois',
		'IN' => 'Indiana',
		'IA' => 'Iowa',
		'KS' => 'Kansas',
		'KY' => 'Kentucky',
		'LA' => 'Louisiana',
		'ME' => 'Maine',
		'MD' => 'Maryland',
		'MA' => 'Massachusetts',
		'MI' => 'Michigan',
		'MN' => 'Minnesota',
		'MS' => 'Mississippi',
		'MO' => 'Missouri',
		'MT' => 'Montana',
		'NE' => 'Nebraska',
		'NV' => 'Nevada',
		'NH' => 'New Hampshire',
		'NJ' => 'New Jersey',
		'NM' => 'New Mexico',
		'NY' => 'New York',
		'NC' => 'North Carolina',
		'ND' => 'North Dakota',
		'OH' => 'Ohio',
		'OK' => 'Oklahoma',
		'OR' => 'Oregon',
		'PA' => 'Pennsylvania',
		'RI' => 'Rhode Island',
		'SC' => 'South Carolina',
		'SD' => 'South Dakota',
		'TN' => 'Tennessee',
		'TX' => 'Texas',
		'UT' => 'Utah',
		'VT' => 'Vermont',
		'VA' => 'Virginia',
		'WA' => 'Washington',
		'WV' => 'West Virginia',
		'WI' => 'Wisconsin',
		'WY' => 'Wyoming',
		'DC' => 'Washington D.C.'
	);

	$drop	= '';
	$drop	.= '<label for="">State</label>';
	$drop	.= '<div class="state-dropdown"><select id="'.$name.'" name="'.$name.'" required>';
	$drop	.= '<option value="" '.selected( 'none', $current, false ).'>(Select)</option>';
	foreach ( $states as $value => $label ) :
		$drop	.= '<option value="'.$value.'" '.selected( $value, $current, false ).'>'.esc_attr( $label ).'</option>';
	endforeach;
	$drop	.= '</select></div>';
	return $drop;
}
