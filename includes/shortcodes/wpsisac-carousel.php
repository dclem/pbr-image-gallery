<?php

function get_wpsisac_carousel_slider( $atts, $content = null ){

    extract(
		shortcode_atts(
		array(
			"gallery_group"		=> 'gallery01',
			"limit"     		=> '-1',
			"category" 			=> '',
			"captions"			=> 'false',
			"thumbnails"		=> 'true',
			"view" 				=> 'thumbslider',
			'image_size' 		=> 'full',
			"slidestoshow" 		=> '3',
			"slidestoscroll" 	=> '-1',
			"dots"     			=> 'false',
			"arrows"     		=> 'true',
			"autoplay"     		=> 'false',
			"autoplay_interval" => '3000',
			"speed"             => '300',
			"centermode"        => 'true',
			"variablewidth"    	=> 'true',
			"sliderheight"     	=> '500',
			"thumbheight"     	=> '150',
	), $atts));

	$shortcode_views 	= wpsisac_slider_views();
	$limit 				= !empty($limit) 					? $limit 						: '-1';
	$cat 				= (!empty($category)) 				? explode(',', $category) 		: '';
	$slidestoshow 		= !empty($slidestoshow) 			? $slidestoshow 				: 3;
	$slidestoscroll 	= !empty($slidestoscroll) 			? $slidestoscroll 				: 1;
	$view 				= ($view && (array_key_exists(trim($view), $shortcode_views))) ? trim($view) : 'thumbslider';
	$dots 				= ( $dots == 'false' ) 				? 'false' 						: 'true';
	$arrows 			= ( $arrows == 'false' ) 			? 'false' 						: 'true';
	$autoplay 			= ( $autoplay == 'false' ) 			? 'false' 						: 'true';
	$autoplay_interval 	= (!empty($autoplay_interval)) 		? $autoplay_interval 			: 3000;
	$speed 				= (!empty($speed)) 					? $speed 						: 300;
	$sliderheight 		= (!empty($sliderheight)) 			? $sliderheight 				: '';
	$slider_height_css 	= (!empty($sliderheight))			? "style='height:{$sliderheight}px;'" : '';
	$thumbs_height_css 	= (!empty($sliderheight))			? "style='height:{$thumbheight}px;'" : '';
	$centermode 		= ( $centermode == 'false' ) 		? 'false' 						: 'true';
	$variablewidth 		= ( $variablewidth == 'false' ) 	? 'false' 						: 'true';
	$sliderimage_size 	= !empty($image_size) 				? $image_size 					: 'full';

	// Shortcode file
	$view_file_path 	= WPSISAC_VERSION_DIR . '/templates/' . $view . '.php';
	$thumbs_file_path 	= WPSISAC_VERSION_DIR . '/templates/' . 'thumbs.php';
	$caption_file_path 	= WPSISAC_VERSION_DIR . '/templates/' . 'captionslider.php';

	$view_file 		= (file_exists($view_file_path)) ? $view_file_path : '';

	// Enqueus required script
	wp_enqueue_script( 'wpos-slick-jquery' );
	wp_enqueue_script( 'wpsisac-public-script' );

	// Slider configuration
	$slider_conf = compact('slidestoshow','slidestoscroll','dots', 'arrows', 'autoplay', 'autoplay_interval', 'speed', 'rtl', 'centermode' , 'variablewidth');

	ob_start();

	global $post;
	$unique			= wpsisac_get_unique();
	$post_type 		= 'slick_slider';
	$orderby 		= 'post_date';
	// $order 			= 'DESC';
	$order 			= 'ASC';

     $args = array (
            'post_type'      => $post_type,
            'orderby'        => $orderby,
            'order'          => $order,
			'posts_per_page' => $limit,
			'post_status' => array('future', 'publish'),
            );
		if($cat != ""){
            	$args['tax_query'] = array(
				array(
				'taxonomy' => 'wpsisac_slider-category',
				'field' => 'term_id',
				'terms' => $cat)
				);
            }
      	$query 		= new WP_Query($args);
		$post_count = $query->post_count;
		  
	if ( $query->have_posts() ) : ?>

	<div id="wpsisac-slick-wrapper-<?php echo $unique; ?>" class="wpsisac-slick-carousal-wrp wpsisac-clearfix wpsisac-slick-wrapper">

		<div id="wpsisac-main-slider-<?php echo $unique; ?>" class="wpsisac-slick-carousal wpsisac-main-slider <?php echo $design; ?> <?php if($centermode == 'true' && $variablewidth == 'true') { echo 'wpsisac-center variablewidthv'; } elseif($centermode == 'true') { echo 'wpsisac-center';} else { echo 'simplecarousal';} ?>">
			<?php
						while ( $query->have_posts() ) : $query->the_post();
								// Include shortcode html file
									if( $view_file ) {
										include( $view_file );
									}
						endwhile;
					?>
		</div>

		<div id="wpsisac-slick-carousal-<?php echo $unique; ?>" class="wpsisac-carousal-conf wpsisac-hide" data-conf="<?php echo htmlspecialchars(json_encode($slider_conf)); ?>"></div>

		<?php if ($captions != 'true' && $thumbnails === 'true') : ?>

			<div id="" class="slider-nav wpsisac-slick-carousal <?php echo $design; ?> <?php if($centermode == 'true' && $variablewidth == 'true') { echo 'wpsisac-center variablewidthv'; } elseif($centermode == 'true') { echo 'wpsisac-center';} else { echo 'simplecarousal';} ?>">

				<?php
						while ( $query->have_posts() ) : $query->the_post();
								// Include shortcode html file
									if( $thumbs_file_path ) {
										include( $thumbs_file_path );
									}
						endwhile;
					?>

			</div>

		<?php endif; ?>

	</div>

	<?php

    endif;
    wp_reset_query();
	return ob_get_clean();

}

add_shortcode('slick-carousel-slider','get_wpsisac_carousel_slider');