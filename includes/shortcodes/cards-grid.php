<?php

function get_wpsisac_( $atts, $content = null ){

    extract(
		shortcode_atts(
		array(
			"limit"     		=> '-1',
			"category" 			=> '',
			"view" 				=> 'thumbslider',
			'image_size' 		=> 'full',		
			"slidestoshow" 		=> '3',
			"slidestoscroll" 	=> '1',		
			"dots"     			=> 'false',
			"arrows"     		=> 'true',
			"autoplay"     		=> 'false',	
			"autoplay_interval" => '3000',
			"speed"             => '300',
			"centermode"        => 'true',	
			"variablewidth"    	=> 'true',
			"sliderheight"     	=> '500',	
			"thumbheight"     	=> '150',
			"rtl"               => '',
	), $atts));
    

};