<?php

function get_wpsisac_image_grid($atts, $content = null)
{

    ob_start();

    extract(
        shortcode_atts(
            array(
                "limit" => '-1',
                "gallery_group" => 'image-gallery',
            ), $atts));

    global $post;

    $args = array(
        'post_type' => 'pbrart',
        'paged' => '1',
        'posts_per_page' => '20',
        'orderby' => 'date',
        'post_status' => array('publish'),
        'order' => 'DESC',
    );

    $query = new WP_Query($args);

    $view_file = WPSISAC_VERSION_DIR . '/templates/image-grid.php';

    include $view_file;

    wp_reset_query();
    return ob_get_clean();

};

function load_more_handler()
{

    global $post;

    if ($_POST['year'] && $_POST['category'] != 'all') {

        $args = array(

            'post_type' => 'pbrart',
            'post_status' => array('publish'),
            'meta_query' => array(

                array(
                    'key' => 'art_year',
                    'value' => $_POST['year'],
                    'compare' => '=',
                ),

                array(
                    'key' => 'art_type',
                    'value' => $_POST['category'],
                    'compare' => '=',
                ),

            ),

            'paged' => $_POST['page'],
            'posts_per_page' => '20',
            'orderby' => 'date',
            'order' => 'DESC',
        );

    } else if ($_POST['year'] && $_POST['category'] == 'all') {

        $args = array(
            'post_type' => 'pbrart',
            'post_status' => array('publish'),
            'meta_query' => array(

                array(
                    'key' => 'art_year',
                    'value' => $_POST['year'],
                    'compare' => '=',
                ),

            ),

            'paged' => $_POST['page'],
            'posts_per_page' => '20',
            'orderby' => 'date',
            'order' => 'DESC',
        );

    }

    $query = new WP_Query($args);

    $html = "";

    $remove[] = "'";
    $remove[] = '"';

    if ($query->have_posts()):

        while ($query->have_posts()): $query->the_post();

            $title = str_replace($remove, "", get_the_title());

            $year = get_post_meta($post->ID, 'art_year', true);
            $type = get_post_meta($post->ID, 'art_type', true);

            $artist = str_replace($remove, "", get_post_meta($post->ID, 'artist_name', true));

            $content = the_content();

            $img = wpsisac_get_post_featured_image($post->ID, 'large', true);

            $html .= '<a class="modal-img" data-src="' . $img . '" data-caption=" <h3 style=\'margin:0;\'>' . $title . '</h3><div>' . $artist . '</div><div>' . $type . '</div><div>' . $year . '</div>" rel="' . $_POST['rel'] . '" data-fancybox="' . $_POST['rel'] . '">';

            $html .= '<div class="grid-item">';
            $html .= '<img src="' . $img . '" />';
            $html .= '</div>';
            $html .= '</a>';

            $img = "";

        endwhile;

    endif;

    echo $html;

    wp_reset_query();

    die();

}

add_action('wp_ajax_nopriv_load_more', 'load_more_handler');
add_action('wp_ajax_load_more', 'load_more_handler');

add_shortcode('art-image-grid', 'get_wpsisac_image_grid');
