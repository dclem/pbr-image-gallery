<?php
/**
 * Admin Class
 *
 * Handles the Admin side functionality of plugin
 *
 * @package WP Slick Slider and Image Carousel
 * @since 1.0.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Wpsisac_Admin {

	function __construct() {		

		// Action to add admin menu
		add_action( 'admin_menu', array($this, 'wpsisac_register_menu'), 12 );

		// Admin init process
		add_action( 'admin_init', array($this, 'wpsisac_admin_init_process') );
	}
	
	/**
	 * Function to notification transient
	 * 
	 * @package WP Slick Slider and Image Carousel
	 * @since 1.5
	 */
	function wpsisac_admin_init_process() {
		// If plugin notice is dismissed
	    if( isset($_GET['message']) && $_GET['message'] == 'wpsisac-plugin-notice' ) {
	    	set_transient( 'wpsisac_install_notice', true, 604800 );
	    }
	}
}

$wpsisac_admin = new Wpsisac_Admin();